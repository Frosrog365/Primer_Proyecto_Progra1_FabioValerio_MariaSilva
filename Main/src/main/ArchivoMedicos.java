/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.*;
import javax.swing.JOptionPane;

/**
 *
 * @author Fabio
 */
public class ArchivoMedicos {

    String[] arregloMedico;
    Medicos[] arregloMedicos;

    public ArchivoMedicos() {
        LeerLicencia l = new LeerLicencia();
        l.leerLicencia();
        this.arregloMedicos = new Medicos[Integer.parseInt(l.arregloLicencia[0])];
    }
    

    /**
     * Método para escribir los datos de un médico al archivo de médicos
     *
     * @param nuevo Recibe como parámetro un médico el cual toma sus datos y
     * los inserta en un string
     */
    public void escribirMedico(Medicos nuevo) {

        try {
            File medicos = new File("archivoDoctores.txt");
            FileWriter fw;
            BufferedWriter bw;
            if (medicos.exists()) {
                fw = new FileWriter(medicos,true);
                bw = new BufferedWriter(fw);
                bw.newLine();
                bw.write(nuevo.getNombre()+"%"+nuevo.getCedula()+"%"+nuevo.getFechaNacimiento()
                +"%"+nuevo.getCorreoElectrónico()+"%"+nuevo.getContrasenna());
            } else {
                fw = new FileWriter(medicos);
                bw = new BufferedWriter(fw);
                bw.write(nuevo.getNombre()+"%"+nuevo.getCedula()+"%"+nuevo.getFechaNacimiento()
                +"%"+nuevo.getCorreoElectrónico()+"%"+nuevo.getContrasenna());
            }
            bw.close();
            fw.close();
        } catch (Exception e) {
            System.out.println(e);
        }
        }

    /**
     * Método que consiste en leer los datos de un string correspondientes a los
     * de un médicos e insertarlos en un arreglo para luego poder ser usados.
     */
    public void leerMedico() {
        try {
            File medicos = new File("archivoDoctores.txt");
            if (medicos.exists()) {
                FileReader fr = new FileReader(medicos);
                BufferedReader br = new BufferedReader(fr);
                String lineaDoctor;
                while ((lineaDoctor = br.readLine()) != null) {

                    arregloMedico = (lineaDoctor.split("%"));
                    Medicos m = new Medicos(arregloMedico[0], arregloMedico[1], arregloMedico[2], arregloMedico[3], arregloMedico[4]);
                    for (int i = 0; i < arregloMedicos.length; i++) {
                        if(arregloMedicos[i]==null){
                            arregloMedicos[i]=m;
                            break;
                        }
                    }
                }
            }
        } catch (Exception e) {
            System.out.println(e);
        }
    }
}
