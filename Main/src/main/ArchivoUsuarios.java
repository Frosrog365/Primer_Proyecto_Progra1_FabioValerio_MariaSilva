/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.*;

/**
 *
 * @author Fabio
 */
public class ArchivoUsuarios {

    String[] arregloUsuariosStr;
    Usuario[] arregloUsuarios = new Usuario[10];

    /**
     * Método para escribir los datos de un usuario al archivo de usuarios
     *
     * @param Usuario Recibe como parámetro un médico el cual toma sus datos y
     * los inserta en un string
     */
    public void escribirUsuario(Usuario nuevo) {

        try {
            File usuario = new File("archivoUsuarios.txt");
            FileWriter fw;
            BufferedWriter bw;
            if (usuario.exists()) {
                fw = new FileWriter(usuario, true);
                bw = new BufferedWriter(fw);
                bw.newLine();
                bw.write(nuevo.getNombre()+"%"+nuevo.getCedula()+"%"+nuevo.getContraseña()
                +"%"+nuevo.getNacimiento()+"%"+nuevo.getUsuario());
            } else {
                fw = new FileWriter(usuario);
                bw = new BufferedWriter(fw);
                bw.write(nuevo.getNombre()+"%"+nuevo.getCedula()+"%"+nuevo.getContraseña()
                +"%"+nuevo.getNacimiento()+"%"+nuevo.getUsuario());
            }
            bw.close();
            fw.close();
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    /**
     * Método que consiste en leer los datos de un string correspondientes a los
     * de un usuario e insertarlos en un arreglo para luego poder ser usados.
     */
    public void leerUsuario() {
        try {
            File usuario = new File("archivoUsuarios.txt");
            if (usuario.exists()) {
                FileReader fr = new FileReader(usuario);
                BufferedReader br = new BufferedReader(fr);
                String lineaUsuario;
                while ((lineaUsuario = br.readLine()) != null) {

                    arregloUsuariosStr = (lineaUsuario.split("%"));
                    Usuario m = new Usuario(arregloUsuariosStr[0], arregloUsuariosStr[1], arregloUsuariosStr[2], arregloUsuariosStr[3], arregloUsuariosStr[4]);
                    for (int i = 0; i < arregloUsuarios.length; i++) {
                        if(arregloUsuarios[i]==null){
                            arregloUsuarios[i]=m;
                            break;
                    }
                    }
                }
            }
        } catch (Exception e) {
            System.out.println(e);
        }
    }
}
