/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;

/**
 *
 * @author Fabio
 */
public class ArchivoSecretaria {

    String[] arregloSecretaria;
    Secretaria[] arregloSecretarias;

/**
 * Método para escribir los datos de una secretaria al archivo de médicos
 *
 * @param Secretaria Recibe como parámetro una secretaria el cual toma sus datos
 * y los inserta en un string
 */
public void escribirSecretarias(Secretaria nuevo) {
        

        try {
            File secretarias = new File("archivoSecretaria.txt");
            FileWriter fw;
            BufferedWriter bw;
            if (secretarias.exists()) {
                fw = new FileWriter(secretarias, true);
                bw = new BufferedWriter(fw);
                bw.newLine();
                bw.write(nuevo.getNombre() + "%" + nuevo.getCedula() + "%"
                        + nuevo.getFechaNacimiento() + "%" + nuevo.getCorreoElectrónico()
                        + "%" + nuevo.getContrasenna());
            } else {
                fw = new FileWriter(secretarias);
                bw = new BufferedWriter(fw);
                bw.write(nuevo.getNombre() + "%" + nuevo.getCedula() + "%"
                        + nuevo.getFechaNacimiento() + "%" + nuevo.getCorreoElectrónico()
                        + "%" + nuevo.getContrasenna());
            }
            bw.close();
            fw.close();
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    /**
     * Método que consiste en leer los datos de un string correspondientes a los
     * de una secretaria e insertarlos en un arreglo para luego poder ser
     * usados.
     */
    public void leerSecretaria() {
        try {
            File secretarias = new File("archivoSecretarias.txt");
            if (secretarias.exists()) {
                FileReader fr = new FileReader(secretarias);
                BufferedReader br = new BufferedReader(fr);
                String lineasecretaria;
                while ((lineasecretaria = br.readLine()) != null) {

                    arregloSecretaria = (lineasecretaria.split("%"));
                    Secretaria s = new Secretaria(arregloSecretaria[0], arregloSecretaria[1], arregloSecretaria[2], arregloSecretaria[3], arregloSecretaria[4]);
                    for (int i = 0; i < arregloSecretarias.length; i++) {
                        if(arregloSecretarias[i]==null){
                            arregloSecretarias[i]=s;
                            break;
                    }
                    }
                }
            }
        } catch (Exception e) {
            System.out.println(e);
        }
    }
}
