/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

/**
 *
 * @author maria
 */
public class Citas {
    private String dia;
    private String mes;
    private String anno;
    private String hora;
    private String cedulapaciente;
    private String doctor;
    private String cedulaDoctor;
    private String nombrePaciente;
    
    public Citas(String dia, String mes, String anno, String hora, String cedulapaciente, String nombrePaciente,String doctor,String cedulaDoctor) {
        this.dia = dia;
        this.mes = mes;
        this.anno = anno;
        this.hora = hora;
        this.cedulapaciente = cedulapaciente;
        this.nombrePaciente = nombrePaciente;
        this.doctor = doctor;
        this.cedulaDoctor = cedulaDoctor;
    }

    public String getNombrePaciente() {
        return nombrePaciente;
    }

    public Citas() {

    }

    public String getDia() {
        return dia;
    }

    public String getMes() {
        return mes;
    }

    public String getAnno() {
        return anno;
    }

    public String getHora() {
        return hora;
    }

    public String getCedulapaciente() {
        return cedulapaciente;
    }

    public String getDoctor() {
        return doctor;
    }

    public String getCeduladoctor() {
        return cedulaDoctor;
    }

    public void setDia(String dia) {
        this.dia = dia;
    }

    public void setMes(String mes) {
        this.mes = mes;
    }

    public void setAnno(String anno) {
        this.anno = anno;
    }

    public void setHora(String hora) {
        this.hora = hora;
    }

    public void setCedulapaciente(String cedulapaciente) {
        this.cedulapaciente = cedulapaciente;
    }

    public void setDoctor(String doctor) {
        this.doctor = doctor;
    }

    public void setCeduladoctor(String ceduladoctor) {
        this.cedulaDoctor = ceduladoctor;
    }

}
