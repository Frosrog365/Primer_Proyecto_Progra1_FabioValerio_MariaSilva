/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.*;
import javax.swing.JOptionPane;

/**
 *
 * @author Fabio_2
 */
public class ArchivoCitas {

    String[] arregloCitasStr;
    Citas[] arregloCitas;

    public ArchivoCitas() {
        LeerLicencia l = new LeerLicencia();
        l.leerLicencia();
        this.arregloCitas = new Citas[Integer.parseInt(l.arregloLicencia[3])];
    }

    /**
     * Método para escribir los datos de una cita al archivo de citas
     *
     * @param nuevo Recibe como parámetro una cita el cual toma sus datos y los
     * inserta en un string
     */
    public void escribirCita(Citas nuevo) {

        try {
            File citas = new File("archivoCitas.txt");
            FileWriter fw;
            BufferedWriter bw;
            if (citas.exists()) {
                fw = new FileWriter(citas,true);
                bw = new BufferedWriter(fw);
                bw.newLine();
                bw.write(nuevo.getDia() + "%" + nuevo.getMes() + "%" + nuevo.getAnno() + "%"
                        + nuevo.getHora() + "%" + nuevo.getCedulapaciente() + "%" + nuevo.getNombrePaciente()
                        + "%" + nuevo.getDoctor() + "%" + nuevo.getCeduladoctor());
            } else {
                fw = new FileWriter(citas);
                bw = new BufferedWriter(fw);
                bw.write(nuevo.getDia() + "%" + nuevo.getMes() + "%" + nuevo.getAnno() + "%"
                        + nuevo.getHora() + "%" + nuevo.getCedulapaciente() + "%" + nuevo.getNombrePaciente()
                        + "%" + nuevo.getDoctor() + "%" + nuevo.getCeduladoctor());
            }
            bw.close();
            fw.close();
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    /**
     * Método que consiste en leer los datos de un string correspondientes a los
     * de una cita e insertarlos en un arreglo para luego poder ser usados.
     */
    public void leerCita() {
        try {
            File citas = new File("archivoCitas.txt");
            if (citas.exists()) {
                FileReader fr = new FileReader(citas);
                BufferedReader br = new BufferedReader(fr);
                String lineaCita;
                while ((lineaCita = br.readLine()) != null) {

                    arregloCitasStr = (lineaCita.split("%"));
                    Citas nuevaCita = new Citas(arregloCitasStr[0], arregloCitasStr[1], arregloCitasStr[2], arregloCitasStr[3], arregloCitasStr[4],
                            arregloCitasStr[5], arregloCitasStr[6], arregloCitasStr[7]);
                    for (int i = 0; i < arregloCitas.length; i++) {
                        if (arregloCitas[i] == null) {
                            arregloCitas[i] = nuevaCita;
                            break;
                        }
                    }
                }
            }
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    /**
     * Método para que dependiento de cada cédula de doctor se agrege la cita a
     * ese doctor
     */
    public void agregarCitasDoctor(ArchivoMedicos aM, ArchivoCitas aC) {
        aM.leerMedico();
        aC.leerCita();
        Citas[] arregloCitasDoctor = new Citas[2000];
        int cont = 0;
        int j = 0;
        int x = 0;
        int i = 0;
        while(i<aC.arregloCitas.length){
            if (aC.arregloCitas[i] != null) {
                if (aC.arregloCitas[i].getDoctor().equals(aM.arregloMedicos[j].getNombre())) {
                    arregloCitasDoctor[cont] = aC.arregloCitas[x];
                    cont++;
                    x++;
                    i++;
                } else {
                    while (i < aM.arregloMedicos.length) {
                        j++;
                        i=0;
                        break;
                    }
                }
            }
            else{
                i++;
            }
        }

        aM.arregloMedicos[j].setListaCitas(arregloCitasDoctor);
    }
}
