/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;

/**
 *
 * @author Fabio
 */
public class ArchivoPacientes {

    String[] arregloPacientes;

    /**
     * Método para escribir los datos de una secretaria al archivo de médicos
     *
     * @param nuevo Tipo paciente el cual se recibe de la lista de pacientes
     * para luego se escrita en el archivo de pacientes
     */
    public void escribirPacientes(Paciente nuevo) {

        try {
            File pacientes = new File("archivoPacientes.txt");
            FileWriter fw;
            BufferedWriter bw;
            if (pacientes.exists()) {
                fw = new FileWriter(pacientes, true);
                bw = new BufferedWriter(fw);
                bw.newLine();
                bw.write(nuevo.getCedula() + "%" + nuevo.getNombre() + "%" + nuevo.getDiadecita()+"%"+
                        nuevo.getMesDeCita()+"%"+nuevo.getAnnoDeCita()+"%"+nuevo.getHoradeCita());
            } else {
                fw = new FileWriter(pacientes);
                bw = new BufferedWriter(fw);
                bw.write(nuevo.getCedula() + "%" + nuevo.getNombre() + "%" + nuevo.getDiadecita()+"%"+
                        nuevo.getMesDeCita()+"%"+nuevo.getAnnoDeCita()+"%"+nuevo.getHoradeCita());
            }
            bw.close();
            fw.close();
        } catch (Exception e) {
            System.out.println(e);
        }
    }

}
