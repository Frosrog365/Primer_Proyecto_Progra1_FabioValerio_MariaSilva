/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

/**
 *
 * @author maria
 */
public class Usuario {

    private String nombre;
    private String cedula;
    private String contraseña;
    private String nacimiento;
    private String tipousuario;

    public Usuario(String nombre, String cedula, String contraseña, String nacimiento, String usuario) {
        this.nombre = nombre;
        this.cedula = cedula;
        this.contraseña = contraseña;
        this.nacimiento = nacimiento;
        this.tipousuario = usuario;
    }

    public String getNombre() {
        return nombre;
    }

    public String getCedula() {
        return cedula;
    }

    public String getContraseña() {
        return contraseña;
    }

    public String getNacimiento() {
        return nacimiento;
    }

    public String getUsuario() {
        return tipousuario;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public void setCedula(String cedula) {
        this.cedula = cedula;
    }

    public void setContraseña(String contraseña) {
        this.contraseña = contraseña;
    }

    public void setNacimiento(String nacimiento) {
        this.nacimiento = nacimiento;
    }

    public void setUsuario(String usuario) {
        this.tipousuario = usuario;
    }

}
