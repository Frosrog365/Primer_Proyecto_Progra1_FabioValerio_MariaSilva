/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

import javax.swing.JOptionPane;

/**
 *
 * @author maria
 */
public class Medicos {

    private String nombre;
    private String cedula;
    private String fechaNacimiento;
    private String correoElectrónico;
    private String contrasenna;
    public Citas[] listaCitas;

    public Medicos(String nombre, String cedula, String fechaNacimiento, String correoElectrónico, String contrasenna) {
        this.nombre = nombre;
        this.cedula = cedula;
        this.fechaNacimiento = fechaNacimiento;
        this.correoElectrónico = correoElectrónico;
        this.contrasenna = contrasenna;
        this.listaCitas = new Citas[48];
    }

    public Citas[] getListaCitas() {
        return listaCitas;
    }

    public void setListaCitas(Citas[] cita) {
        this.listaCitas = cita;
    }

    public Medicos() {
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getCedula() {
        return cedula;
    }

    public void setCedula(String cedula) {
        this.cedula = cedula;
    }

    public String getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(String fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    public String getCorreoElectrónico() {
        return correoElectrónico;
    }

    public void setCorreoElectrónico(String correoElectrónico) {
        this.correoElectrónico = correoElectrónico;
    }

    public String getContrasenna() {
        return contrasenna;
    }

    public void setContrasenna(String contrasenna) {
        this.contrasenna = contrasenna;
    }

    @Override
    public String toString() {
        return "Medicos{" + "nombre=" + nombre + ", cedula=" + cedula + ", fechaNacimiento=" + fechaNacimiento + ", correoElectr\u00f3nico=" + correoElectrónico + ", contrasenna=" + contrasenna + '}';
    }

    /**
     * Método para que el médico pueda consultar sus citas del día
     */
    public void ConsultarCitas() {
        ArchivoCitas aC = new ArchivoCitas();
        ArchivoMedicos aM = new ArchivoMedicos();
        aM.leerMedico();
        aC.agregarCitasDoctor(aM, aC);
        int cont = 0;
        for (int i = 0; i < aM.arregloMedicos.length; i++) {

            if (aM.arregloMedicos[i].getNombre().equals(this.nombre)) {
                String dia = JOptionPane.showInputDialog("Dígite el día de la cita");
                String mes = JOptionPane.showInputDialog("Digite el mes de la cita");
                String anno = JOptionPane.showInputDialog("Digite el año de la cita");
                for (int j = 0; j < aM.arregloMedicos[i].listaCitas.length; j++) {
                    if (aM.arregloMedicos[i].listaCitas[j] != null) {
                        if ((aM.arregloMedicos[i].listaCitas[j].getDia()).equals(dia)
                                && aM.arregloMedicos[i].listaCitas[j].getMes().equals(mes)
                                && aM.arregloMedicos[i].listaCitas[j].getAnno().equals(anno)) {
                            JOptionPane.showMessageDialog(null, aM.arregloMedicos[i].listaCitas[j].getCedulapaciente() + "\n"
                                    + aM.arregloMedicos[i].listaCitas[j].getNombrePaciente() + "\n" + aM.arregloMedicos[i].listaCitas[j].getHora() + ":00\n");
                        } else {
                            JOptionPane.showMessageDialog(null, "Este médico no tiene citas para esa fecha");
                            break;
                        }
                    }

                }
                break;

            }
        }

    }

    /**
     * Método para Registrar las citas
     */
    public void RegistrarCitas() {
        ArchivoCitas aC = new ArchivoCitas();
        aC.leerCita();
        int cont = 0;
        for (int i = 0; i < this.listaCitas.length; i++) {
            if (this.listaCitas[i] != null) {
                cont++;
            }
        }
        if (cont == this.listaCitas.length) {
            System.out.println("La cantidad de citas para ese doctor está completa");
        } else {
            int dia = Integer.parseInt(JOptionPane.showInputDialog("Digite el día"));
            int mes = Integer.parseInt(JOptionPane.showInputDialog("Digite el mes"));
            int anno = Integer.parseInt(JOptionPane.showInputDialog("Digite el año"));
            String nombre = JOptionPane.showInputDialog("Digite el nombre del paciente");
            String cedulaPaciente = JOptionPane.showInputDialog("Digite el nombre la cédula del paciente");
            String hora = JOptionPane.showInputDialog("Digite la hora de la paciente");
            Citas nuevaCita = new Citas(Integer.toString(dia), Integer.toString(mes), Integer.toString(anno), hora, cedulaPaciente, nombre, this.getNombre(), cedula);
            Paciente nuevoPaciente = new Paciente(cedula, nombre, Integer.toString(dia), Integer.toString(mes), Integer.toString(anno),hora);
            aC.escribirCita(nuevaCita);
            for (int i = 0; i < listaCitas.length; i++) {
                if (listaCitas[i] == null) {
                    listaCitas[i] = nuevaCita;
                    break;
                }
            }
        }

    }

    public void Receta() {
        ArchivoCitas aC = new ArchivoCitas();
        ArchivoMedicos aM = new ArchivoMedicos();
        aM.leerMedico();
        aC.agregarCitasDoctor(aM, aC);
        for (int i = 0; i < aM.arregloMedicos.length; i++) {
            
        }
    }

}
