/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

import java.util.ArrayList;
import javax.swing.JOptionPane;

/**
 *
 * @author Fabio_2
 */
public class Login {

    public void menu() {
        ArchivoMedicos aM = new ArchivoMedicos();
        ArchivoUsuarios aU = new ArchivoUsuarios();
        aM.leerMedico();
        ArchivoSecretaria aS = new ArchivoSecretaria();
        aS.leerSecretaria();
        

        while (true) {
            int opcionMenuPrincipal = Integer.parseInt(JOptionPane.showInputDialog("1. Registrarse\n" + "2. Iniciar Sesión\n" + "3. Salir\n" + "Digite una opción"));
            if (opcionMenuPrincipal == 1) {
                int cont = 0;
                for (int i = 0; i < aM.arregloMedicos.length; i++) {

                    if (aM.arregloMedicos[i] != null) {
                        cont++;
                    }

                }
                if (cont == aM.arregloMedicos.length) {
                    System.out.println("La lista de medicos está llena");
                } else {
                    String cedula = JOptionPane.showInputDialog("Digite su cédula");
                    String nombre = JOptionPane.showInputDialog("Digite su nombre completo");
                    String fechaNacimiento = JOptionPane.showInputDialog("Digite su fecha de nacimiento");
                    String correoElectronico = JOptionPane.showInputDialog("Digite su correo electrónico");
                    String tipoUsuario = JOptionPane.showInputDialog("Digite su Tipo de Usuario(Médico/Secretaria)");
                    String contrasenna = JOptionPane.showInputDialog("Digite su contraseña");

                    if ("Médico".equals(tipoUsuario) || "medico".equals(tipoUsuario) || "Medico".equals(tipoUsuario) || "médico".equals(tipoUsuario)) {
                        Medicos nuevoM = new Medicos(nombre, cedula, fechaNacimiento, correoElectronico, contrasenna);
                        Usuario nuevo = new Usuario(nombre, cedula, contrasenna, fechaNacimiento, tipoUsuario);
                        aU.escribirUsuario(nuevo);
                        aM.escribirMedico(nuevoM);
                    } else if ("Secretaria".equals(tipoUsuario) || "secretaria".equals(tipoUsuario)) {
                        Secretaria nuevoS = new Secretaria(nombre, cedula, fechaNacimiento, correoElectronico, contrasenna);
                        
                        Usuario nuevo = new Usuario(nombre, cedula, contrasenna, fechaNacimiento, tipoUsuario);
                        aU.escribirUsuario(nuevo);
                        aS.escribirSecretarias(nuevoS);
                    } else {
                        JOptionPane.showMessageDialog(null, "Tipo de usuario Inválido");
                    }
                }
            } else if (opcionMenuPrincipal == 2) {
                aM.leerMedico();
                aS.leerSecretaria();
                
                String cedula = JOptionPane.showInputDialog("Digite su cedula:");
                String contrasenna = JOptionPane.showInputDialog("Digite su contraseña:");

                for (int i = 0; i < aM.arregloMedicos.length; i++) {
                    if (aM.arregloMedicos[i] != null) {
                        if ((cedula.equals(aM.arregloMedicos[i].getCedula()) && contrasenna.equals(aM.arregloMedicos[i].getContrasenna()))) {
                            JOptionPane.showMessageDialog(null, "Bienvenido Doctor " + aM.arregloMedicos[i].getNombre());
                            MenuMedicos mm = new MenuMedicos();
                            mm.mostrarse(Integer.parseInt(cedula));
                            break;

                        }
                    }
                }
//                 for (int i = 0; i < aS.arregloSecretaria.length; i++) {
//                    if (aS.arregloSecretaria[i] != null) {
//                        if ((cedula.equals(aS.arregloSecretaria[i]) && contrasenna.equals(aS.arregloSecretarias[i].getContrasenna()))) {
//                            JOptionPane.showMessageDialog(null, "Bienvenida Secretaria " + aS.arregloSecretarias[i].getNombre());
//                            MenuSecretarias mS = new MenuSecretarias();
//                            mS.mostrar();
//                            break;
//                        }
//                    }
//                 }
                for (int i = 0; i < aM.arregloMedicos.length; i++) {
                    if (aM.arregloMedicos[i] != null) {
                        if (cedula.equals(aM.arregloMedicos[i].getCedula()) && !contrasenna.equals(aM.arregloMedicos[i].getContrasenna())) {
                            System.out.println("Contraseña Incorrecta");
                        }
                    }

                }

            } else if (opcionMenuPrincipal == 3) {
                JOptionPane.showMessageDialog(null, "Gracias por su uso");
                break;

            }

        }

    }

//     public static void iniciarSeccion(){
//         ArrayList<String> listaAdministradores = new ArrayList();
//          listaAdministradores.add("fabio" + " 1234");
//         String nombre=JOptionPane.showInputDialog("Digite su nombre:");
//         String contraseÃ±a=JOptionPane.showInputDialog("Digite su contraseÃ±a:");
//         String linea;
//            for (int i = 0; i < listaAdministradores.size(); i++) {
//                if (nombre.equals(listaAdministradores.get(i).split(" ")[0]) && contraseÃ±a.equals(listaAdministradores.get(i).split(" ")[1])) {
//                    System.out.println("");
//                    break;
//                    
//                }
//                else{
//                    System.out.println("No ");
//                }
//            }
//    
//}
}
