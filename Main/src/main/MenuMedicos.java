/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

import java.util.Scanner;
import javax.swing.JOptionPane;

/**
 *
 * @author maria
 */
public class MenuMedicos {

    public void mostrarse(int cedulaDoctor) {
        ArchivoMedicos aM = new ArchivoMedicos();
        aM.leerMedico();
        while (true) {

            int opcion = Integer.parseInt(JOptionPane.showInputDialog("1. Consultar Citas\n"
                    + "2. Registrar Citas\n"
                    + "3. Atender Paciente\n"
                    + "4. Receta"
                    + "5. Cerrar Sesión\n"
                    + "Digite una opcion"));
            if (opcion == 1) {
                for (int i = 0; i < aM.arregloMedicos.length; i++) {
                    if (Integer.parseInt(aM.arregloMedicos[i].getCedula()) == cedulaDoctor) {
                        aM.arregloMedicos[i].ConsultarCitas();
                        break;
                    }
                }
            }
            if (opcion == 2) {

                for (int i = 0; i < aM.arregloMedicos.length; i++) {
                    if (Integer.parseInt(aM.arregloMedicos[i].getCedula()) == cedulaDoctor) {
                        aM.arregloMedicos[i].RegistrarCitas();
                        break;
                    }
                }
            }
            if (opcion == 3) {
                for (int i = 0; i < aM.arregloMedicos.length; i++) {
                    if (Integer.parseInt(aM.arregloMedicos[i].getCedula()) == cedulaDoctor) {
                        aM.arregloMedicos[i].Receta();
                        break;
                    }
                }
            }

            if (opcion == 5) {
                break;

            }
        }
    }

}


