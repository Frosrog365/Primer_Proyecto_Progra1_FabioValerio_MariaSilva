/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

/**
 *
 * @author maria
 */
public class Registrar {
     private String nombre;
    private String apellidouno;
    private String apellidodos;
    private String cedula;
    private String contraseña;
    private String nacimiento;
    private String sexo;
    private String direccion;

    public Registrar(String nombre, String apellidouno, String apellidodos, String cedula, String contraseña, String nacimiento, String sexo, String direccion) {
        this.nombre = nombre;
        this.apellidouno = apellidouno;
        this.apellidodos = apellidodos;
        this.cedula = cedula;
        this.contraseña = contraseña;
        this.nacimiento = nacimiento;
        this.sexo = sexo;
        this.direccion = direccion;
    }
    
    public Registrar(){
        //inicio
        nombre="Digite su nombre:";
        apellidouno="Digite su primer apellido:";
        apellidodos="Digite su segundo apellido:";
        cedula="Digite su identificacion:";
        contraseña="Digite su contraseña:";
        nacimiento="Digite su fecha de nacimiento:";
        sexo="Digite su sexo:";
        direccion="Digite su direccion:";
    }

    public String getNombre() {
        return nombre;
    }

    public String getApellidouno() {
        return apellidouno;
    }

    public String getApellidodos() {
        return apellidodos;
    }

    public String getCedula() {
        return cedula;
    }

    public String getContraseña() {
        return contraseña;
    }

    public String getNacimiento() {
        return nacimiento;
    }

    public String getSexo() {
        return sexo;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public void setApellidouno(String apellidouno) {
        this.apellidouno = apellidouno;
    }

    public void setApellidodos(String apellidodos) {
        this.apellidodos = apellidodos;
    }

    public void setCedula(String cedula) {
        this.cedula = cedula;
    }

    public void setContraseña(String contraseña) {
        this.contraseña = contraseña;
    }

    public void setNacimiento(String nacimiento) {
        this.nacimiento = nacimiento;
    }

    public void setSexo(String sexo) {
        this.sexo = sexo;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    @Override
    public String toString() {
        return "Registrar{" + "nombre=" + nombre + ", apellidouno=" + apellidouno + ", apellidodos=" + apellidodos + ", cedula=" + cedula + ", contrase\u00f1a=" + contraseña + ", nacimiento=" + nacimiento + ", sexo=" + sexo + ", direccion=" + direccion + '}';
    }
    
    
}
